var express = require('express'); 
var router = express.Router(); 

// app.get(route, callback)
router.get('/', function(req, res) { 
    res.send('Hello World!'); 
}); 

router.get("/me", function(req, res) { 
    res.send('I am Bharat Dong.'); 
}); 

// curl -X POST "http://localhost:3000/me"
router.post('/me', function(req, res){
    res.send("My nickname is zenius lama\n");
 });

 // A special method, all, is provided by Express to 
 // handle all types of http methods at a particular route 
 //using the same function. 
 router.all('/test', function(req, res) { 
    res.send('This is a test page.\n'); 
 }); 

router.get("/json", function(req, res) { 
    res.json({name: 'bharat', age: '26'}); 
});

 module.exports = router; 