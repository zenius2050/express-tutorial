var express = require('express'); 
var dynamicRouter = express.Router(); 

dynamicRouter.get('/:id', function(req, res) {
  //  console.log(req.params); 
    res.send(`Your requested id is ${req.params.id}`); 
}); 

// pattern matched routes
dynamicRouter.get('/:name([a-zA-Z]+)/:age([0-9]+)', function(req, res) {
    var { name , age } = req.params;  
    res.send(`Name: ${name}, Age: ${age}`); 
}); 

module.exports = dynamicRouter; 