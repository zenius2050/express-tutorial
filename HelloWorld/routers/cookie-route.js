var express = require("express"),
    cookieRouter = express.Router(); 

cookieRouter.get("/", function(req, res) { 
    console.log('Cookies: ', req.cookies);
   // res.cookie('name', 'cookie').send("cookie demo"); 
   
   var name ="name"; 
   // This cookie  expires after 360000 ms from the time it is set.
    res.cookie(name, 'value', { maxAge: 360000 }).send("cookie demo");
}); 


cookieRouter.get("/clear", function(req, res) { 
    console.log('Cookies: ', req.cookies); 
    res.clearCookie(Object.keys(req.cookies)[0]);  // pass cookie 'key' here 
    res.send(`cookie '${req.cookies.name}' cleared`); 
}); 

module.exports = cookieRouter; 