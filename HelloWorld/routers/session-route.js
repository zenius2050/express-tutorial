var express = require("express"),
    sessionRouter = express.Router(); 

sessionRouter.get("/", function(req, res) { 
    var pageViews = req.session.pageViews; 
    if(pageViews) { 
       pageViews++; 
       req.session.pageViews = pageViews; 
       res.send(`You visited this page ${pageViews} times.`); 
    } else { 
        req.session.pageViews = 1; 
        res.send('You visited this page for the first time.'); 
    }
}); 

module.exports = sessionRouter;