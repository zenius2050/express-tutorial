var express = require('express'); 
var User = require("../models/user");

var formRouter = express.Router();

formRouter.get("/login", function(req, res){ 
    res.render("../views/form-view");  
});

// curl -X POST "http://localhost:3000/form"
formRouter.post("/", function(req, res){ 
    console.log(req.body); 
    res.send('received your request\n'); 
}); 

formRouter.get("/signup", function(req, res) { 
    res.render("../views/signup-view"); 
}); 

formRouter.post("/show-message", function(req, res) { 
    var user = req.body; 
    console.log(user); 
    if(!user.name || !user.password) { 
        res.render('../views/show-message-view', {
            message: "Sorry, you provided wrong info",
            type: "error"
        }); 
    } else { 
        var newUser = new User({ 
            name: user.name, 
            password: user.password
        }); 

        newUser.save(function(error, result){ 
          //  console.log('result ',result); 

            if(error) { 
                res.render('../views/show-message-view', {
                    message: "database error",
                    type: "error"
                }); 

            } else { 
                res.render('../views/show-message-view', {
                    message: "New user added", 
                    type: "success", 
                    user: user
                });
            }

        }); 
    }
}); 


module.exports = formRouter; 