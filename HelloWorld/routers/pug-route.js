var express = require('express'); 
var pugRouter = express.Router(); 

pugRouter.get("/first-view", function(req, res) { 
    res.render('first-view'); 
}); 

pugRouter.get("/dynamic-view", function(req, res) { 
    var params = {
        linkText: 'Go to Pug site', 
        url: "https://pugjs.org"
    }
    res.render('dynamic-view', { params }); 
}); 

pugRouter.get("/conditional-view", function(req, res) { 
    var user =  { 
        name: 'Bharat', 
        url: "https://pugjs.org"
    }; 

    res.render('conditional-view', {user}); 
}); 

pugRouter.get("/component-view", function(req, res) { 
    res.render('../views/component-view'); 
}); 

pugRouter.get("/test-view", function(req, res) { 
    res.render('../views/test-view'); 
}); 

module.exports = pugRouter; 