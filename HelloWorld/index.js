var express = require('express'); 
var bodyParser =  require('body-parser'); 
var cookieParser = require("cookie-parser"); 
var multer = require('multer'); 
var mongoose = require('mongoose'); 
var session = require('express-session'); 

mongoose.connect('mongodb://localhost/my-db', {useNewUrlParser: true});

// import routes
var router = require('./routers/router'); 
var dynamicRouter = require('./routers/dynamic-route'); 
var pugRouter = require("./routers/pug-route"); 
var formRouter = require("./routers/form-route"); 
var cookieRouter = require("./routers/cookie-route"); 
var sessionRouter = require('./routers/session-route'); 

// initialize application
var app = express();
var upload = multer(); 

app.set('view engine', 'pug');  // set the pug as the templating engine 
app.set('views', './views'); // set the path for templating engine

app.use(bodyParser.urlencoded({ extended: true })); // to parse url encoded data : application/xwww-
app.use(bodyParser.json()); // to parse json data : application/json
app.use(cookieParser()); 
app.use(upload.array()); // to parse multipart/form-data
app.use("/static", express.static('public'));  // virtual path prefix 
app.use(session({   
                    secret: 'express secret' , 
                    resave: false,
                    saveUninitialized: false
                })
        ); 

// basic request time logger 
app.use(function(req, res, next) { 
    console.log(`A new request received at ${new Date().toTimeString()}`); 
    next(); 
});

// initialize routes
app.use(router); 
app.use('/dynamic', dynamicRouter); 
app.use("/pug", pugRouter); 
app.use("/form",formRouter); 
app.use("/cookie", cookieRouter); 
app.use("/session", sessionRouter); 

// error handling 
app.get('*', function(req, res){
    res.send('404 , url not valid');
 });


app.listen(3000, console.log('server is listening on port 3000')); 